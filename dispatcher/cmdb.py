"""
Interface to SnipeIt, the CMDB.
"""

import requests
import json
from dispatcher.host import EnterpriseLinuxHost, DebianHost


def assets(url, key):
    """
    Retrieve a list of all systems from the CMDB.

    The list is filtered: only systems with settings for
    - MAC address
    - operating system
    - partitioning information
    are returned.

    :param url: URL to the SnipeIt API
    :param key: access key for authentication
    :return: list of host objects
    """
    # a = snipeit.Assets()
    # hosts = a.get(server=url, token=key)
    headers = {
        "content-type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer {0}".format(key),
    }
    try:
        results = requests.get(
            url, headers=headers, verify=False  # nosec - ETHZ-CA verification issue
        )
    except requests.exceptions.ConnectionError:
        return "No connection to SnipeIt", 400

    if results.status_code != 200:
        result = "Code from SnipeIT: {0}".format(results.status_code)
        return result, results.status_code

    hosts = []
    result = json.loads(results.content)
    for line in result["rows"]:
        try:
            mac = line["custom_fields"]["MAC-Adresse"]["value"]
        except KeyError:
            continue

        try:
            os_full = line["custom_fields"]["OS"]["value"]
        except KeyError:
            os_full = None
            continue
        if os_full is None:
            continue

        try:
            partitioning = line["custom_fields"]["Partitionierung"]["value"]
        except KeyError:
            partitioning = None
            continue
        if partitioning is None:
            continue

        try:
            # 'Redhat 9', 'Debian 12' etc.
            os, release = os_full.split(" ")
        except ValueError:
            continue

        if line["custom_fields"]["MAC-Adresse"]["value"] is not None:
            host_init = None
            if (
                os.lower() == "redhat"
                or os.lower() == "almalinux"  # noqa W503
                or os.lower() == "rocky"  # noqa W503
            ):
                host_init = EnterpriseLinuxHost
            if os.lower() == "debian":
                host_init = DebianHost

            if host_init:
                h = host_init(
                    mac, os=os.lower(), release=release, partitioning=partitioning
                )
                hosts.append(h)

    return hosts, results.status_code
