"""
Wrapper class for a system to be installed.
"""

import dispatcher.unattended

debian_releases = {12: "bookworm"}
el_distros = ["redhat", "rocky", "almalinux"]
debian_distros = ["debian", "ubuntu"]


class Host:
    """
    Object representing an installable system.
    """

    os = ""
    family = ""
    release = "8"
    service = ""
    ip = ""
    netmask = ""
    gateway = ""
    dns = ""
    hostname = ""
    domain = ""
    partitioning = ""

    def __init__(self, mac, os=None, release=None, partitioning=None):
        """
        Create a new host object.

        :param mac: MAC address (mandatory)
        :param os: operating system (`name` from config lookup table `available_os`)
        :param release: release, must also correspond to lookup table
        :param partitioning: value from table `partition_mapping`
        """
        self.mac = mac
        if os is not None:
            self.os = os
            if os.lower() in el_distros:
                self.family = "redhat"
            if os.lower() in debian_distros:
                self.family = "debian"
        if os is not None:
            self.os = os
        if release is not None:
            self.release = release
        if partitioning is not None:
            self.partitioning = partitioning

    def render_ipxe(self, config):
        """
        Render iPXE menu file from templates
        :param config: configuration object
        :return: plaintext string with iPXE menu content
        """
        ipxe = dispatcher.unattended.IpxeFile()
        return ipxe.render(config, self)

    def render_installfile(self, config):
        raise NotImplementedError


class DebianHost(Host):
    release_name = ""

    def __init__(self, mac, os, release, partitioning):
        Host.__init__(self, mac, os, release, partitioning)
        self.release_name = debian_releases[int(release)]

    def render_installfile(self, config):
        preseed = dispatcher.unattended.PreseedFile()
        return preseed.render(config, self)


class EnterpriseLinuxHost(Host):
    def render_installfile(self, config):
        """
        Render kickstart file from templates
        :param config: configuration object
        :return: plaintext string with kickstart file content
        """
        ks = dispatcher.unattended.KickstartFile()
        return ks.render(config, self)
