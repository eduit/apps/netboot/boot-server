"""
A HTTP server providing iPXE menu and kickstart files for automated setups of hosts.

For host configuration information, a CMDB (SnipeIt) is used to select operating system
or partitioning schemes.

bgiger@ethz.ch 2023
"""
from flask import Flask, request, make_response
import logging
import re
import dispatcher.configuration
import dispatcher.dispatch
import socket

# Read and evaluate configuration settings. Missing settings will cause abortion before app creation.
conf = dispatcher.configuration.ConfigFile("config.json")
app = Flask(__name__)


@app.route("/")
def hello():
    """
    Root path does nothing spectacular.
    :return: hello message
    """
    return "KS dispatcher running"


@app.route("/healthz")
def healthz():
    """
    Standard path for health checks
    :return: a friendly message
    """
    return "OK"


@app.route("/ipxe", methods=["GET"])
def ipxe():
    """
    Request to prepare and return an iPXE menu. Required HTTP argument: `mac=`. Will only return
    a valid file if a matching asset is found in the CMDB.
    :return: iPXE menu for this host, if possible
    """
    args = request.args
    if args.get("mac") is None:
        logging.error("MAC address missing in request")
        return "MAC address missing in request", 400
    p = re.compile("^((([a-f0-9]{2}:){5})|(([a-f0-9]{2}-){5}))[a-f0-9]{2}$")
    if p.match(args.get("mac")) is None:
        logging.error("Malformed MAC address")
        return "Malformed MAC address", 400

    content, code = dispatcher.dispatch.ipxe(args, conf)
    if code != 200:
        logging.error(content)
        return "Error during processing: {0}".format(content), code

    response = make_response(content, 200)
    response.mimetype = "text/plain"
    return response


@app.route("/ks", methods=["GET"])
def kickstart():
    """
    Request to prepare and return a kickstart file. Required HTTP argument: see `expected` list.
    Will only return a valid file if a matching asset is found in the CMDB (again, matching MAC
    address).

    If the `hostname` is missing during PXE start (happens in our environment, for unknown reason),
    a reverse DNS lookup with the IP address is attempted.

    :return: kickstart file, if possible
    """
    expected = ["mac", "ip", "netmask", "dns", "hostname", "domain", "gateway"]
    args = request.args
    # Do the checks
    # Existence
    for item in expected:
        if args.get(item) is None:
            logging.error("Missing in request: {0}".format(item))
            return "Missing in request: {0}".format(item), 400

    # MAC address
    p = re.compile("^((([a-f0-9]{2}:){5})|(([a-f0-9]{2}-){5}))[a-f0-9]{2}$")
    if p.match(args.get("mac")) is None:
        logging.error("Malformed MAC address")
        return "Malformed MAC address", 400

    # IP address, netmask
    # Bogus IPs like 10.0.0.0 would pass, but it's about sanitizing
    p = re.compile(
        "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"  # noqa W605 E501
    )
    if p.match(args.get("ip")) is None:
        logging.error("Malformed IP address")
        return "Malformed IP address", 400
    if p.match(args.get("netmask")) is None:
        logging.error("Malformed netmask")
        return "Malformed netmask", 400
    if p.match(args.get("dns")) is None:
        logging.error("Malformed DNS address")
        return "Malformed DNS address", 400
    if p.match(args.get("gateway")) is None:
        logging.error("Malformed gateway address")
        return "Malformed gateqay address", 400

    # Hostname
    hostname = ""
    p = re.compile(
        "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$"  # noqa W605 E501
    )
    # iPXE may not always get the hostname, fallback is DNS resolution here
    if p.match(args.get("hostname")) is None:
        logging.warning("Malformed hostname, trying to resolve")
        fqdn, dummy = socket.getnameinfo((args.get("ip"), 0), 0)
        if fqdn is not None:
            # TODO: better canonical name determination
            hostname = fqdn.split(".")[0]
        else:
            return "Could not determine hostname", 400
    else:
        hostname = args.get("hostname")

    content, code = dispatcher.dispatch.ks(args, conf, hostname)
    if code != 200:
        logging.error(content)
        return "Error during processing: {0}".format(content), code

    response = make_response(content, 200)
    response.mimetype = "text/plain"
    return response


if __name__ == "__main__":  # pragma: nocover
    app.run()
