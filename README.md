# Netboot Dispatcher

Dynamically create kickstart files.

## Purpose

Reads host configurations from SnipeIt and uses custom fields

- `OS`: ie. "Redhat 8", "Debian 12" (name - space - release)
- `MAC-Adresse`
- `Partitionierung`: see configuration

together with network information retrieved by `iPXE` from DHCP, like IP address or default gateway.

Requests to this service can retrieve an iPXE menu and kickstart/preseed files.

Upon the first reboot, a post installation callback to the Semaphore Ansible server initiates a configuration run.

## Configuration

### Configuration File
The configuration file `config.json` in the working directory defines addresses of the SnipeIt server or
kickstart repositories, in a list of available OS versions. It also provides mappings from partitioning names
in SnipeIt to partitioning templates. See also `config_example.json`.

The Semaphore server configuration includes the server name, and project and template IDs (can found in the
Semaphore URL of an Ansible template).

### Server URL
The iPXE menu file contains a reference to this service. Since the server name cannot be determined with
confidence, it has to be configured. It may be configured in the configuration file or set with an
environment variable `SERVER_URL`.

### API Keys
Access to SnipeIt requires an API key. Create a key in SnipeIt and define the environment variable
`SNIPEIT_API_KEY`.

For the Semaphore callback, a `SEMAPHORE_API_KEY` variable has to be set.

### SnipeIt
Hosts must have these mandatory attributes configured:
- MAC address
- operating system (as "`os` `release`")
- partitioning

If an attribute is missing, the host definition is not accepted for further processing.

### Partitioning Schemes
The configuration key `partition_schemes` is a hash (dict) with the SnipeIt names for partitioning
schemes as keys. For each family of operating systems, one dict has to be configured.

For Redhat type OSes, the values are arrays of lines, each line is a partitioning statement. The values will
be concatenated and inserted in the kickstart file. This format has been chosen to improve readability,
since JSON would require one single line with `\n` inserted, which becomes awkward quickly.

## Usage
### iPXE Menu
A GET request to `/ipxe` requires the argument `mac` address, the response is a iPXE script.

### Kickstart/Preseed File
Issue a GET request to the web service endpoint `/ks` with the required arguments, listed here with their iPXE names:

- `mac`: `${net0/mac}`
- `ip`: `${ip}`
- `netmask`: `${netmask}`
- `hostname`: `${hostname}`
- `domain`: `${netX/domain}`
- `dns`: `${netX/dns}`
- `gateway`: `${netX/gateway}`

The response is a plain text kickstart or preseed file.

## Semaphore Callback Template
The Ansible playbook for the callback must not limit the hosts allowed to be configured. Use `hosts: all` as filter,
as the callback will set the limit to the hostname of the installed host.

## Extension
Kickstart templates are Jinja2 templates named *<os>*_kickstart.ks (or *<os>*_preseed.cfg). Partitioning specific
templates extend the general template *<os>*_kickstart.ks, the base template has a Jinja2 block called
`partitioning` which is overwritten in the more specific templates.

### More OS Variants
Create a new template like the one provided. The `os` part of the name is taken from the
SnipeIt attribute (set to lowercase).
